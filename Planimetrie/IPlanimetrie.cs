﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Planimetrie
{
    public interface IPlanimetrie
    {
        abstract double Fläche()
        {
            return 0;
        }

        abstract double Umfang()
        {
            return 0;
        }

        abstract public event EventHandler<EventArgs> GeößeGeändert;
    }
}
