﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Planimetrie
{
    public abstract class PlanObject : IPlanimetrie
    {
        abstract double Fläche()
        {
            return 0;
        }

        abstract double Umfang()
        {
            return 0;
        }

        protected void WennGrößeGeändert(EventArgs e)
        {

        }

        public event EventHandler<EventArgs> GeößeGeändert;
    }
}
