﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Planimetrie
{
    public class Rechteck : PlanObject
    {
        double SeiteA
        {
            get;
            set
            {
                if (value < 0)
                {
                    throw new ArgumentOutOfRangeException();
                }
                if (value != SeiteA)
                {
                    SeiteA = value;
                    WennGrößeGeändert(EventArgs.Empty);
                }
            }
        }
        double SeiteB { get; set; }

        double Fläche()
        {
            return 0;
        }

        double Umfang()
        {
            return 0;
        }
    }
}
