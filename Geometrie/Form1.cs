﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Geometrie
{
    public partial class Form1 : Form
    {
        private RechteckControl rechteckControl;

        public GraphObjekt GraphObjekt
        {
            get;
            set;
        }

        public Form1()
        {
            InitializeComponent();
            GraphObjekt = new GraphRechteck(rechteckControl.Rechteck);
            GraphObjekt.PositionGeändert += GraphObjekt_PositionGeändert;
            rechteckControl.Rechteck.GrößeGeändert += GraphObjekt_GrößeGeändert;
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            GraphObjekt.Zeichnen(Pens.Black, e.Graphics);
        }
    }
}
