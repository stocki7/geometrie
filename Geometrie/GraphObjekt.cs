﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using Planimetrie;

namespace Geometrie
{
    public abstract class GraphObjekt : IPosition, IZeichnung
    {
        public IPlanimetrie PlanObjekt
        {
            get;
            private set;
        }

        public event EventHandler<EventArgs> PositionGeändert;

        public float X
        {
            get;
            set
            {
            }
        }

        public float Y
        {
            get;
            set
            {
            }
        }

        public void Bewegen(float deltaX, float deltaY)
        {
            throw new System.NotImplementedException();
        }

        public void BewegenNach(float neuesX, float neuesY)
        {
            throw new System.NotImplementedException();
        }

        protected void WennPositionGeändert(EventArgs e)
        {

        }

        protected GraphObjekt(IPlanimetrie planObjekt)
        {
            throw new System.NotImplementedException();
        }

        public abstract void Zeichnen(Pen p, Graphics g)
        {
            throw new System.NotImplementedException();
        }
    }
}
