﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Planimetrie;

namespace Geometrie
{
    public partial class RechteckControl : UserControl
    {
        public Rechteck Rechteck
        {
            get;
            private set;
        }
        public RechteckControl()
        {
            InitializeComponent();
            Rechteck = new Rechteck();
        }

        private void nudSeiteA_ValueChanged(object sender, EventArgs e)
        {
            Rechteck.SeiteA = (double)nudSeiteA.Value;
        }

        private void nudSeiteB_ValueChanged(object sender, EventArgs e)
        {
            Rechteck.SeiteB = (double)nudSeiteB.Value;
        }
    }
}
