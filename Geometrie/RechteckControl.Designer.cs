﻿namespace Geometrie
{
    partial class RechteckControl
    {
        /// <summary> 
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Komponenten-Designer generierter Code

        /// <summary> 
        /// Erforderliche Methode für die Designerunterstützung. 
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.nudSeiteA = new System.Windows.Forms.NumericUpDown();
            this.nudSeiteB = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.nudSeiteA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSeiteB)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(28, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "SeiteA";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(207, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "SeiteB";
            // 
            // nudSeiteA
            // 
            this.nudSeiteA.Location = new System.Drawing.Point(69, 17);
            this.nudSeiteA.Name = "nudSeiteA";
            this.nudSeiteA.Size = new System.Drawing.Size(120, 20);
            this.nudSeiteA.TabIndex = 2;
            this.nudSeiteA.ValueChanged += new System.EventHandler(this.nudSeiteA_ValueChanged);
            // 
            // nudSeiteB
            // 
            this.nudSeiteB.Location = new System.Drawing.Point(248, 17);
            this.nudSeiteB.Name = "nudSeiteB";
            this.nudSeiteB.Size = new System.Drawing.Size(120, 20);
            this.nudSeiteB.TabIndex = 3;
            this.nudSeiteB.ValueChanged += new System.EventHandler(this.nudSeiteB_ValueChanged);
            // 
            // RechteckControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.nudSeiteB);
            this.Controls.Add(this.nudSeiteA);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "RechteckControl";
            this.Size = new System.Drawing.Size(415, 49);
            ((System.ComponentModel.ISupportInitialize)(this.nudSeiteA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSeiteB)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown nudSeiteA;
        private System.Windows.Forms.NumericUpDown nudSeiteB;
    }
}
