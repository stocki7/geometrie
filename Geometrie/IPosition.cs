﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geometrie
{
    public interface IPosition
    {
        float X { get; }
        float Y { get; }

        void Bewegen(float deltaX, float deltaY)
        {
            
        }

        void BewegenNach(float neuesX, float neuesY)
        {

        }

        public event EventHandler<EventArgs> PositionGeändert;
    }
}
